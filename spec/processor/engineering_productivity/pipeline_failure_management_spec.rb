# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/pipeline_failure_management'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PipelineFailureManagement do
  include_context 'with event', 'Triage::PipelineEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: true
      }
    end
  end

  let(:slack_webhook_url) { 'slack_webhook_url' }

  before do
    stub_env('SLACK_WEBHOOK_URL', slack_webhook_url)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['pipeline.failed']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when no config is found' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(true)
        allow(Triage::PipelineFailure::ConfigSelector)
          .to receive(:find_config).with(event, described_class::CONFIGURATION_CLASSES)
          .and_return(nil)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:config) { instance_double(Triage::PipelineFailure::Config::MasterBranch) }
    let(:failed_jobs) { instance_double(Triage::PipelineFailure::FailedJobs) }
    let(:incident) { double('Incident') }
    let(:incident_creator) { instance_double(Triage::PipelineFailure::IncidentCreator, execute: incident) }
    let(:slack_notifier) { instance_double(Triage::PipelineFailure::SlackNotifier, execute: true) }

    before do
      allow(Triage::PipelineFailure::ConfigSelector)
        .to receive(:find_config).with(event, described_class::CONFIGURATION_CLASSES)
        .and_return(config)

      allow(Triage::PipelineFailure::FailedJobs)
        .to receive(:new).with(event).and_return(failed_jobs)
      allow(failed_jobs).to receive(:execute).and_return([])

      allow(Triage::PipelineFailure::IncidentCreator)
        .to receive(:new)
        .and_return(incident_creator)
      allow(Triage::PipelineFailure::SlackNotifier)
        .to receive(:new)
        .and_return(slack_notifier)
    end

    shared_examples 'Slack posting' do
      it 'posts to Slack' do
        expect(Triage::PipelineFailure::SlackNotifier)
          .to receive(:new)
          .with(
            event: event,
            config: config,
            failed_jobs: [],
            slack_webhook_url: slack_webhook_url,
            incident: incident
          ).and_return(slack_notifier)
        expect(slack_notifier).to receive(:execute).and_return(incident)

        subject.process
      end
    end

    context 'when config.create_incident? == true' do
      before do
        allow(config).to receive(:create_incident?).and_return(true)
      end

      it 'creates an incident' do
        expect(Triage::PipelineFailure::IncidentCreator)
          .to receive(:new).with(event: event, config: config, failed_jobs: [])
          .and_return(incident_creator)
        expect(incident_creator).to receive(:execute).and_return(incident)

        subject.process
      end

      it_behaves_like 'Slack posting'
    end

    context 'when config.create_incident? == false' do
      let(:incident) { nil }

      before do
        allow(config).to receive(:create_incident?).and_return(false)
      end

      it 'does not create an incident' do
        expect(incident_creator).not_to receive(:execute)

        subject.process
      end

      it_behaves_like 'Slack posting'
    end
  end
end
