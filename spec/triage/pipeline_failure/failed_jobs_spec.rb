# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/failed_jobs'

RSpec.describe Triage::PipelineFailure::FailedJobs do
  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_id: 999)
  end

  subject { described_class.new(event) }

  describe '#execute' do
    let(:first_job_name) { 'foo' }
    let(:second_job_name) { 'a' }
    let(:third_job_name) { 'baz' }
    let(:api_client_double) { double('API client') }
    let(:pipeline_jobs) { double('pipeline_jobs') }
    let(:pipeline_bridges) { double('pipeline_bridges') }

    before do
      allow(Triage).to receive(:api_client).and_return(api_client_double)
      allow(pipeline_jobs).to receive(:auto_paginate)
        .and_yield(double(name: first_job_name, allow_failure: false))
        .and_yield(double(name: second_job_name, allow_failure: false))
      allow(pipeline_bridges).to receive(:auto_paginate)
        .and_yield(double(name: 'bar', allow_failure: true))
        .and_yield(double(name: third_job_name, 'web_url=': true, web_url: true, downstream_pipeline: double(web_url: 'baz_web_url'), allow_failure: false))
    end

    it 'fetches the failed jobs and return the ones that are not allowed to fail' do
      expect(Triage.api_client).to receive(:pipeline_jobs)
        .with(event.project_id, event.id, scope: 'failed', per_page: 100)
        .and_return(pipeline_jobs)
      expect(Triage.api_client).to receive(:pipeline_bridges)
        .with(event.project_id, event.id, scope: 'failed', per_page: 100)
        .and_return(pipeline_bridges)

      expect(subject.execute.map(&:name)).to eq([first_job_name, second_job_name, third_job_name])
    end
  end
end
