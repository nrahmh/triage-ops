# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/triage_incident'

RSpec.describe Triage::PipelineFailure::TriageIncident do
  let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
  let(:event)      { instance_double(Triage::PipelineEvent, id: 123, project_id: 278964) }
  let(:job1)       { double(id: '1', name: 'job_a', web_url: 'a_web_url', allow_failure: false) }
  let(:job2)       { double(id: '2', name: 'job_b', web_url: 'b_web_url', allow_failure: false) }
  let(:job3)       { double(id: '3', name: 'job_c', web_url: 'c_web_url', allow_failure: false) }
  let(:job4)       { double(id: '4', name: 'job_d', web_url: 'd_web_url', allow_failure: false) }
  let(:job5)       { double(id: '5', name: 'job_e', web_url: 'e_web_url', allow_failure: false) }

  subject { described_class.new(event, failed_jobs) }

  describe '#root_cause_analysis_comment' do
    let(:api_client_double)           { double('gitlab_api_client') }
    let(:fixture_path)                { '/reactive/job_traces' }
    let(:failed_to_pull_image_trace)  { read_fixture("#{fixture_path}/failed_to_pull_image.txt") }
    let(:infra_trace)                 { read_fixture("#{fixture_path}/infrastructure_error.txt") }
    let(:gitlab_com_overloaded_trace) { read_fixture("#{fixture_path}/gitlab_com_overloaded.txt") }
    let(:runner_disk_full_trace)      { read_fixture("#{fixture_path}/runner_disk_full.txt") }

    before do
      allow(Triage).to receive(:api_client).and_return(api_client_double)
      allow(api_client_double).to receive(:job_trace).with(project_id, '1').and_return(failed_to_pull_image_trace)
      allow(api_client_double).to receive(:job_trace).with(project_id, '2').and_return(infra_trace)
      allow(api_client_double).to receive(:job_trace).with(project_id, '3').and_return(gitlab_com_overloaded_trace)
      allow(api_client_double).to receive(:job_trace).with(project_id, '5').and_return(runner_disk_full_trace)

      allow(api_client_double).to receive(:job_retry).and_return({ 'web_url' => 'retry_job_web_url' })
      allow(api_client_double).to receive(:retry_pipeline).and_return({ 'web_url' => 'retried_pipeline_web_url' })

      # trace not recognized
      allow(api_client_double).to receive(:job_trace).with(project_id, '4').and_return(
        <<~TEXT
          Finished in 1 minute 46.1 seconds (files took 46.62 seconds to load)
          1 example, 1 failure
          Failed examples:
          rspec ./spec/features/users/login_spec.rb:304
        TEXT
      )
    end

    context 'with 10 failed jobs, all due to a known transient error' do
      let(:failed_jobs) { Array.new(10) { job1 } }

      it 'determines root cause from trace, retries pipeline, and closes incident' do
        expect(Triage.api_client).to receive(:job_trace).exactly(10).times
        expect(Triage.api_client).to receive(:retry_pipeline).once
        expect(Triage.api_client).not_to receive(:job_retry)

        expect(described_class.new(event, failed_jobs).root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".

            Retried pipeline: retried_pipeline_web_url

            This incident is caused by known transient error(s), closing.
            /close
            /label ~"master-broken::failed-to-pull-image"
          MARKDOWN
        )
      end
    end

    context 'with 10 failed jobs, some have unknown root causes' do
      let(:failed_jobs) { (Array.new(9) { job1 }).push(job4) }

      it 'labels incident with the top root cause, does not retry pipeline, does not closes incident' do
        expect(Triage.api_client).to receive(:job_trace).exactly(10).times
        expect(Triage.api_client).not_to receive(:retry_pipeline)
        expect(Triage.api_client).not_to receive(:job_retry)

        expect(described_class.new(event, failed_jobs).root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_d](d_web_url): ~"master-broken::undetermined".
            /label ~"master-broken::failed-to-pull-image"
          MARKDOWN
        )
      end
    end

    context 'with less than 10 failed jobs' do
      it 'fetches all traces and retries failed jobs individually with job_retry requests' do
        failed_jobs = Array.new(9) { job1 }

        expect(Triage.api_client).to receive(:job_trace).exactly(9).times
        expect(Triage.api_client).to receive(:job_retry).exactly(9).times
        expect(Triage.api_client).not_to receive(:retry_pipeline)

        expect(described_class.new(event, failed_jobs).root_cause_analysis_comment).not_to include(
          'Retried pipeline: retried_pipeline_web_url'
        )
      end

      context 'with no failed job (e.g. if the CI config is invalid)' do
        let(:failed_jobs) { [] }

        it 'constructs triage notes based on the root cause of the only job failure' do
          expect(subject.root_cause_analysis_comment).to eq(%(\n/label ~"master-broken::undetermined"))
        end
      end

      context 'with 1 failed job due to a known transient error' do
        let(:failed_jobs) { [job2] }

        it 'labels the incident based on the failed job root cause, retries the job and closes incident' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.
              /close
              /label ~"master-broken::infrastructure"
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected with some repeated causes and some non-transient errors' do
        let(:failed_jobs) { [job1, job1, job2, job3, job4, job5] }

        it 'labels the incident based on the top root cause, and retries each job failed with transient error' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_e](e_web_url): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url
              /label ~"master-broken::failed-to-pull-image"
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected, all caused by transient errors' do
        let(:failed_jobs) { [job1, job2, job3, job3, job5] }

        it 'labels the incident with the top root cause, retries each job and closes the incident' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_e](e_web_url): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.
              /close
              /label ~"master-broken::gitlab-com-overloaded"
            MARKDOWN
          )
        end
      end

      context 'when none of the job failures are recognized' do
        let(:failed_jobs) { Array.new(4) { job4 } }

        it 'constructs triage notes, does not retry any job and labels the incident with undetermined root cause' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              /label ~"master-broken::undetermined"
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected, top one being "undetermined"' do
        let(:failed_jobs) { [job4, job4, job4, job3, job3, job1] }

        it 'constructs triage notes, retries the jobs with transient errors, and labels the incident with the 2nd highest root cause' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              /label ~"master-broken::gitlab-com-overloaded"
            MARKDOWN
          )
        end
      end

      context 'when every job has a different root cause, not all errors are transient' do
        let(:failed_jobs) { [job1, job2, job3, job4, job5] }

        it 'constructs triage notes and labels the incident with any root cause besides undetermined' do
          expect(subject.root_cause_analysis_comment).not_to include(
            %(/label ~"master-broken::undetermined")
          )
        end
      end
    end
  end
end
