# frozen_string_literal: true

require 'json'
require 'slack-messenger'

module Triage
  module PipelineFailure
    class SlackNotifier
      MESSAGE_AUTHOR = 'Failed pipeline reporter'
      MESSAGE_ICON_EMOJI = ':boom:'

      def initialize(event:, config:, slack_webhook_url:, failed_jobs: [], incident: nil)
        @event = event
        @config = config
        @slack_webhook_url = slack_webhook_url
        @failed_jobs = failed_jobs
        @incident = incident
      end

      def execute
        json = format(config.slack_payload_template, **template_variables)
        messenger.ping(
          username: MESSAGE_AUTHOR,
          icon_emoji: MESSAGE_ICON_EMOJI,
          **JSON.parse(json))
      end

      def messenger
        Slack::Messenger.new(slack_webhook_url, config.slack_options)
      end

      private

      attr_reader :event, :config, :slack_webhook_url, :failed_jobs, :incident

      def template_variables
        {
          title: title,
          project_link: project_link,
          pipeline_link: pipeline_link,
          incident_button_text: incident_button_text,
          incident_button_link: incident_button_link,
          branch_link: branch_link,
          commit_link: commit_link,
          triggered_by_link: triggered_by_link,
          source: source,
          pipeline_duration: pipeline_duration,
          failed_jobs_count: failed_jobs.size,
          failed_jobs_list: failed_jobs_list,
          merge_request_link: merge_request_link
        }
      end

      def title
        "#{project_link} pipeline #{pipeline_link} failed"
      end

      def project_link
        "<#{event.project_web_url}|#{event.project_path_with_namespace}>"
      end

      def pipeline_link
        "<#{event.web_url}|##{event.id}>"
      end

      def incident_button_text
        if incident
          "View incident ##{incident.iid}"
        else
          'Create incident'
        end
      end

      def incident_button_link
        if incident
          incident.web_url
        else
          "#{event.project_web_url}/-/issues/new?" \
            "issuable_template=incident&issue%5Bissue_type%5D=incident"
        end
      end

      def branch_link
        "<#{event.project_web_url}/-/commits/#{event.ref}|`#{event.ref}`>"
      end

      def pipeline_duration
        ((Time.now - event.created_at) / 60.to_f).round(2)
      end

      def commit_link
        "<#{event.project_web_url}/-/commit/#{event.sha}|#{event.commit_header}>"
      end

      def source
        "`#{event.source}#{schedule_type}`"
      end

      def schedule_type
        event.source == 'schedule' ? ": #{variable_value('SCHEDULE_TYPE')}" : ''
      end

      def variable_value(key)
        variable = event.variables.find { |h| h['key'] == key }
        return unless variable

        variable['value']
      end

      def triggered_by_link
        # Recreate the server URL from event.project_web_url...
        "<#{event.project_web_url.delete_suffix(event.project_path_with_namespace)}/#{event.event_actor.username}|#{event.event_actor.name}>"
      end

      def failed_jobs_list
        failed_jobs.map { |job| "<#{job.web_url}|#{job.name}>" }.join(', ')
      end

      def merge_request_link
        return 'N/A' unless event.merge_request

        "<#{event.merge_request.web_url}|#{event.merge_request.title}>"
      end
    end
  end
end
