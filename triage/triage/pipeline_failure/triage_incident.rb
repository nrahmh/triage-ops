# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class TriageIncident
      attr_reader :failed_jobs, :event

      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      LABELS_FOR_TRANSIENT_ERRORS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
        :failed_to_pull_image,
        :gitlab_com_overloaded,
        :runner_disk_full,
        :infrastructure
      ).freeze

      ROOT_CAUSE_TO_TRACE_MAP = {
        failed_to_pull_image: ['job failed: failed to pull image'],
        gitlab_com_overloaded: ['gitlab is currently unable to handle this request due to load'],
        runner_disk_full: ['no space left on device'],
        infrastructure: [
          'the requested url returned error: 5', # any 5XX error code should be transient
          'error: downloading artifacts from coordinator',
          'error: uploading artifacts as "archive" to coordinator'
        ]
      }.freeze

      POST_RETRY_JOB_URL_THRESHOLD = 10

      def initialize(event, failed_jobs)
        @failed_jobs = failed_jobs
        @event = event
      end

      def root_cause_analysis_comment
        comments = list_all_triaged_jobs_comment + retry_pipeline_comment + close_incident_comment
        <<~MARKDOWN.chomp
          #{comments}
          /label ~"#{top_root_cause_label}"
        MARKDOWN
      end

      private

      def list_all_triaged_jobs_comment
        triaged_jobs.map do |triaged_job|
          %(- #{triaged_job[:link]}: ~"#{triaged_job[:label]}".#{retry_job_comment(triaged_job)})
        end.join("\n")
      end

      def triaged_jobs
        @triaged_jobs ||=
          failed_jobs.map do |job|
            job_trace = ci_job(job.id).trace
            root_cause = determine_root_cause_from_trace(job_trace)

            {
              id: job.id,
              link: "[#{job.name}](#{job.web_url})",
              label: ROOT_CAUSE_LABELS[root_cause]
            }
          end
      end

      def retry_job_comment(failed_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(failed_job)
        retry_job_web_url.nil? ? '' : " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(triaged_job)
        return unless transient_error?(triaged_job[:label]) && retry_jobs_individually?

        ci_job(triaged_job[:id]).retry['web_url']
      end

      def transient_error?(root_cause_label)
        LABELS_FOR_TRANSIENT_ERRORS.include?(root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        failed_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        if all_jobs_failed_with_transient_errors? && !retry_jobs_individually?
          retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id).to_h

          "\n\nRetried pipeline: #{retry_pipeline_response['web_url']}"
        else
          ''
        end
      end

      def close_incident_comment
        if all_jobs_failed_with_transient_errors?
          "\n\nThis incident is caused by known transient error(s), closing.\n/close"
        else
          ''
        end
      end

      def all_jobs_failed_with_transient_errors?
        return false if triaged_jobs.empty?

        (potential_labels_root_cause_labels.uniq - LABELS_FOR_TRANSIENT_ERRORS).empty?
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if triaged_jobs.empty?

        # find the top root cause label preferrably not master-broken::undetermined
        potential_labels_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def potential_labels_root_cause_labels
        @potential_labels_root_cause_labels ||= triaged_jobs.map { |job| job[:label] }
      end

      def ci_job(job_id)
        Triage::CiJob.new(event.project_id, job_id)
      end

      def determine_root_cause_from_trace(job_trace)
        ROOT_CAUSE_TO_TRACE_MAP.detect do |root_cause, trace_patterns|
          trace_patterns.any? { |pattern| job_trace.downcase.include?(pattern) }
        end&.first || :default
      end
    end
  end
end
